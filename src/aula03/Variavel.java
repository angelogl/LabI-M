/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula03;

import aula02.Calculadora;

/**
 *
 * @author angelogl
 */
public class Variavel {
    public static void main(String[] args) {
        
        System.out.println("##Calculadora##");
        System.out.println("10x5 = "+10*5);
        System.out.println("10/5 = "+10/5);
        System.out.println("32+98 = "+32+98);
        System.out.println("32-98 = "+(32-98));
        
        
        System.out.println("CASE 2");
        int valor1 = 10;
        int valor2 = 5;
        System.out.println(valor1+"x"+valor2+" = "+(valor1*valor2));
        
        System.out.printf("%dx%d = %d\n",valor1,valor2,(valor1*valor2));
        System.out.printf("%d/%d = %d\n",valor1,valor2,(valor1/valor2));
        System.out.printf("%d+%d = %d\n",valor1,valor2,(valor1+valor2));
        System.out.printf("%d-%d = %d\n",valor1,valor2,(valor1-valor2));
        
        long valor= 10000L;
        
        short valor3 = 222;
        
        boolean valor4 = true;
        boolean valor5 = false;
        
        double valor6 = 666.888;
        float valor7 = (float) 55.99;
        System.out.println(" TESTE CHAR ");
        int valor8 = 'A';
        System.out.println(valor8);
        valor8 = 66;
        System.out.println(valor8);
        
        String texto = "Laboratório I";
        
        // <, >, <=, >=, !=, ==
        System.out.println(!(9<8));
        
        // ! && ||
        System.out.println(true && false);
        System.out.println(true || false);
        System.out.println((true || false)&& true);
        
    } 
}
