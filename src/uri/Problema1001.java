/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uri;

import java.util.Scanner;

/**
 *
 * @author angelogl
 */
public class Problema1001 {
    public static void main(String[] args) {
        Scanner teclado = new Scanner (System.in);
        //ENTRADA DE DADOS
        int v1 = teclado.nextInt();
        int v2 = teclado.nextInt();
        //PROCESSAMENTO
        int resultado  = v1+v2;
        
        //SAÍDA DE DADOS
        System.out.println("X = "+resultado);
    }
    
}
