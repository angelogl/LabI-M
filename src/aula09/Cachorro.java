/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula09;

/**
 *
 * @author angelogl
 */
public class Cachorro {

    String raca = "Vira-lata";
    String nome = "Totó";
    String porte= "Mini";
    boolean pedigree=false;
    double peso=1.0;
    String dataDeNascimento="22/05/2017";
    double energia = 100;

    void caminhar() throws InterruptedException {
        energia = energia - 10;
        processarRequisicao();
    }

    void visualizarStatus() throws InterruptedException {
        System.out.println("Raça: " + raca);
        System.out.println("Nome: " + nome);
        System.out.println("Porte: " + porte);
        System.out.println("Peso: " + peso);
        System.out.println("Energia: " + energia);
        processarRequisicao();
    }

    void processarRequisicao() throws InterruptedException {
        System.out.print("Aguarde");
        for (int i = 0; i < 10; i++) {
            System.out.print(".");
            Thread.sleep(500);
        }
        System.out.println("");
    }
}
