/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula09;

/**
 *
 * @author angelogl
 */
public class Principal {
    public static void main(String[] args) {
        Cliente c = new Cliente();
        c.nome = "Angelo Luz";
        c.cpf = "014.035.510-31";
        c.conta = new ContaBancaria();
        c.conta.limite = 1000;
        
        System.out.println(c.nome);
        System.out.println(c.conta.saldo);
    }
}
