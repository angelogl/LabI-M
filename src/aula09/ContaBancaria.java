/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula09;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author angelogl
 */
public class ContaBancaria {
    int agencia;
    int conta;
    double saldo;
    double limite;

    // visibilidade retorno nome (parametros)
    // Visibilidade: public, protected, private e default
    
    void sacar() throws IOException{
        BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
         System.out.print("Digite o valor do saque: ");
                    double valorSaque = Double.parseDouble(teclado.readLine());
                    if(valorSaque > (limite+saldo) ||
                            valorSaque <= 0){
                        System.err.println("Não é possível sacar esta quantidade");
                    }else{
                        saldo = saldo - valorSaque;
                        System.out.println("Retire o seu dinheiro");
                    }
    }

    void depositar() throws IOException {
        BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
          System.out.print("Digite o valor do depósito: ");
                    double valorDeposito = Double.parseDouble(teclado.readLine());
                    if(valorDeposito<=0){
                        System.err.println("Não é possível depositar esta quantidade");
                    }else{
                        saldo = saldo + valorDeposito;
                        System.out.println("Deposito realizado.");
                        System.out.println("Saldo: "+saldo);
                    }
    }

    void verificarSaldo() {
        System.out.println("SALDO: "+saldo);
    }

    void totalDisponivel() {
        System.out.println("TOTAL DISPONÍVEL: "+(saldo+limite));
    }
}
