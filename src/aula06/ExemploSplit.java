/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula06;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author angelogl
 */
public class ExemploSplit {
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        
        // 10   15 -20
        //| 0 | 1 | 2 |
        String linha = teclado.nextLine();
        
        String[] partes = linha.split(" ");
        
        int a = Integer.parseInt(partes[0]);
        int b = Integer.parseInt(partes[1]);
        int c = Integer.parseInt(partes[2]);
        
        
        // Alternativa 1
        int[] valores = new int[3];
        valores[0] = a;
        valores[1] = b;
        valores[2] = c;
        
        // Alternativa 2
        int[] values = {a,b,c};
        // Ordena array
        Arrays.sort(values);
        // Imprimindo array
        System.out.println(values[a]);
        System.out.println(values[1]);
        System.out.println(values[2]);
        System.out.println("");
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        
        
    }
}
