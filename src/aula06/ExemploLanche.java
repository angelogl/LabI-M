/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula06;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author angelogl
 */
public class ExemploLanche {
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        
        int cod = teclado.nextInt();
        int quantidade = teclado.nextInt();
        double valorTotal;
        
        switch (cod) {
            case 1:
                valorTotal = quantidade * 4.00;
                System.out.println(valorTotal);
                
                break;
            case 2:
                valorTotal = quantidade * 4.50;
                System.out.println(valorTotal);
                break;
            case 3:
                valorTotal = quantidade * 5.00;
                System.out.println(valorTotal);
                break;
            case 4:
                valorTotal = quantidade * 2.00;
                System.out.println(valorTotal);
                break;
            case 5:
                valorTotal = quantidade * 1.50;
                System.out.println(valorTotal);
                break;
            default:
                System.out.println("Opção inválida!");
        }
        
        if(cod==1){
            valorTotal = quantidade * 4.00;
        }else if(cod == 2){
            valorTotal = quantidade * 4.50;
        }else if(cod == 3){
            valorTotal = quantidade * 5.00;
        }else if (cod == 4){
            valorTotal = quantidade * 2.00;
        }else if (cod == 5){
            valorTotal = quantidade * 1.50;
        }else{
            valorTotal = quantidade * 0.50;
        }
        
        System.out.printf("Total: R$ %.2f\n",valorTotal);
    }
}
