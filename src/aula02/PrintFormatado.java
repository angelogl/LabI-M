/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula02;

/**
 *
 * @author angelogl
 */
public class PrintFormatado {
    public static void main(String[] args) {
        // Saída em inteiro %d
        System.out.printf("%d\n",12);
        // Saída formatada
        System.out.printf("%f\n",0.3333);
        // Saída formatada e com casas limitadas
        // Com duas casas
        System.out.printf("%.2f\n", 0.33333);
        // Saída formatada e com casas limitadas
        // Com uma casas
        System.out.printf("%.1f\n", 0.33333);
        // String na saída
        System.out.printf("Aula de %s é na %s",
                "Laboratório I", "Quinta-feira");
        
        System.out.printf("O professor %s tem %d anos",
                "Gladimir", 1000);
        
    }
}
