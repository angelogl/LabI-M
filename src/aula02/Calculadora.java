/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula02;

/**
 *
 * @author angelogl
 */
public class Calculadora {
    public static void main(String[] args) {
        System.out.println("Exemplo 1");
        System.out.println("##Calculadora##");
        System.out.print("32x98 = ");
        System.out.println(32*98);
        
        System.out.println("Exemplo 2");
        System.out.println("##Calculadora##");
        System.out.println("32x98 = "+32*98);
        
        
        System.out.println("Exemplo 3");
        System.out.println("##Calculadora##");
        System.out.printf("32x98 = %d\n",32*98);
        
        System.out.println("Exemplo 4");
        System.out.println("##Calculadora##");
        System.out.printf("%dx%d = %d\n",32,98,32*98);
    }
}
