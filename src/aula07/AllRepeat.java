/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula07;

import java.util.Scanner;

/**
 *
 * @author angelogl
 */
// LEIA 3 VALORES E APRESENTE O SEU TOTAL
public class AllRepeat {
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        //Contador
        int contador = 0;
        // Acumulador
        int total = 0;
        System.out.println("LENDO COM DO-WHILE");
        do{
            System.out.println("Digite o valor "+(contador+1));
            int valor = teclado.nextInt();
            total = total + valor;
            contador = contador + 1;
        }while(contador<3);
        System.out.println("TOTAL: "+total);
        
        contador = 0;
        total = 0;
        System.out.println("LENDO NO WHILE...");
        while(contador < 3){
            System.out.println("Digite o valor "+(contador+1));
            int valor = teclado.nextInt();
            total = total + valor;
            contador = contador + 1;
        }
        System.out.println("TOTAL: "+total);
        
        for (int i = 0; i < 3; i++) {
            System.out.println("Digite o valor "+(i+1));
            int valor = teclado.nextInt();
            total = total + valor;
        }
        
    }
}
