package aula07;

import java.util.Scanner;

/**
 *
 * @author angelogl
 */
public class BotecoMussum {

    public static void main(String[] args) {

        // Declaração e leitura de variáveis
        Scanner teclado = new Scanner(System.in);

        System.out.print("Quantidade inicial do estoque de meh: ");
        int meh = teclado.nextInt();

        System.out.print("Quantidade inicial do estoque de cervegis");
        int cervegis = teclado.nextInt();

        System.out.print("Quantidade inicial de cigarris: ");
        int cigarris = teclado.nextInt();
        int escolha = 0;
        
        // QUERO REPETIR
        do{
        System.out.println("#### Boteco do Mussum  ####");
        System.out.println("# 1. Comprar um  Meh      #");
        System.out.println("# 2. Comprar uma cervegis #");
        System.out.println("# 3. Comprar um cigarris  #");
        System.out.println("# 4. Verificar estoquis   #");
        System.out.println("# 9. Sair   #");
        escolha = teclado.nextInt();
        switch (escolha) {
            case 1:
                if (meh > 0) {
                    //Mesmo que: meh = meh -1;
                    meh--;
                    System.out.println("Não vá ficar embriagadis!");
                } else {
                    System.out.println("Acabou o estoquis");
                }
                break;
            case 2:
                if (cervegis > 0) {
                    //Mesmo que: cervegis = cervegis -1;
                    cervegis--;
                    System.out.println("Não vá ficar embriagadis!");
                } else {
                    System.out.println("Acabou o estoquis");
                }
                break;
            case 3:
                if (cigarris > 0) {
                    //Mesmo que: cigarris = cigarris -1;
                    cigarris--;
                    System.out.println("Cigarro compradis");
                } else {
                    System.out.println("Acabou o estoquis");
                }
                break;
            case 4:
                System.out.println("Estoque: ");
                System.out.println("Meh: "+meh+" unidadis");
                System.out.printf("Cervegis %d unidadis\n", cervegis);
                System.out.printf("Cigarris %d unidadis\n", cigarris);
                System.out.println("----------------------");
                break;
            case 9:
                break;
            default:
                System.err.println("Entrada inválidis");
        }
        }while(escolha!=9);
        // ENCERRAR REPETIÇÃO
    }
    
}
