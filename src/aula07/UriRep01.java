/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula07;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Scanner;

/**
 *
 * @author angelogl
 */

// Leia N valores e se o valor for par, imprima.
// O programa finaliza quando encontrar um fim de arquivo (EOF)
public class UriRep01 {
    public static void main(String[] args) throws IOException {
        
        Scanner entrada = new Scanner(System.in);
        
        // Substitui o Scanner
        BufferedReader teclado = 
                new BufferedReader(new InputStreamReader(System.in));
        // Substitui o System.out
        BufferedWriter saida = 
                new BufferedWriter(new OutputStreamWriter(System.out));
        
       while(true){
           String valor = teclado.readLine();
           if(valor == null || valor.equals("")){
               break; // Fará com que saia do While
           }
           int valorInteiro = Integer.parseInt(valor);
           if(valorInteiro%2 == 0){
               //System.out.print(valorInteiro+"\n");
               // Armazena todas as saídas
               saida.write(valorInteiro+"\n");    
           } 
       }
       //  Faz a impressão de todas as saídas
       saida.flush();
    }
}
