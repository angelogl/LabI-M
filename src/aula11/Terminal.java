/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula11;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author angelogl
 */
public class Terminal {

    public static void main(String[] args) throws IOException, InterruptedException {
        ContaBancaria conta = new ContaBancaria();
        conta.limite = 2000.0;
        BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
        int escolha = 0;

        while (true) {
            System.out.println("=== Banco Bradesquis ===");
            System.out.println("1. Sacar");
            System.out.println("2. Depositar");
            System.out.println("3. Verificar Saldo");
            System.out.println("4. Verificar Total Disponível");
            System.out.println("9. Sair");
            System.out.print("Escolha uma opção: ");
            
            escolha = Integer.parseInt(teclado.readLine());
            
            System.out.println("Processando sua requisição");
            for (int i = 0; i < 10; i++) {
                System.out.print(".");
                
                Thread.sleep(100);
            }
            
            System.out.println("");
            if(escolha==9){
                break;
            }
            switch (escolha) {
                case 1:
                    System.out.println("Digite o valor do saque:");
                    double valorSaque = 
                            Double.parseDouble(teclado.readLine());
                   conta.sacar(valorSaque);
                    break;
                case 2:
                    //TODO: Adicionar leitura
                  conta.depositar(22);
                    break;
                case 3:
                    conta.verificarSaldo();
                    break;
                case 4:
                    conta.totalDisponivel();
                    break;
                default:
                    System.err.println("Opção inválida");
            }

        }

    }
}
