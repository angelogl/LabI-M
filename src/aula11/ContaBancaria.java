/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aula11;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author angelogl
 */
public class ContaBancaria {
    int agencia;
    int conta;
    double saldo = 100;
    double limite = 1000;

    // visibilidade retorno nome (parametros)
    // Visibilidade: public, protected, private e default
    String sacar(double valorSaque) {
        //BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));
        // System.out.print("Digite o valor do saque: ");
        //            double valorSaque = Double.parseDouble(teclado.readLine());
                    if(valorSaque > (limite+saldo) ||
                            valorSaque <= 0){
                        return("Não é possível sacar esta quantidade");
                    }else{
                        saldo = saldo - valorSaque;
                        return("Retire o seu dinheiro");
                    }
    }

    String depositar(double valorDeposito) {
        if(valorDeposito<=0){
                        return("Não é possível depositar esta quantidade");
                    }else{
                        saldo = saldo + valorDeposito;
                        return("Deposito realizado.\nSaldo" +saldo);
                    }
    }

    void verificarSaldo() {
        System.out.println("SALDO: "+saldo);
    }

    void totalDisponivel() {
        System.out.println("TOTAL DISPONÍVEL: "+(saldo+limite));
    }
}
